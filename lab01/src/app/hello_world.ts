import {Component} from 'angular2/core';
import {Notification} from './notification';

@Component({

  // Declare the tag name in index.html to where the component attaches
  selector: 'hello-world',
  directives: [Notification],
  template: `
  <div><notification [name]="yourName" [notif]="yourNotif" [pattern]="yourPattern"></notification>   </div>
  <label>your name:</label>
	<input type="text" [(ngModel)]="yourName" placeholder="Enter a name here">
  <label>pattern:</label>
  <input type="text" [(ngModel)]="yourPattern" placeholder="Enter a pattern here">
  <label>notification:</label>
  <input type="text" [(ngModel)]="yourNotif" placeholder="Enter a notification text here">
	<hr> 
	<h1 [hidden]="!yourName">Hello {{yourName}}!</h1>
`
})

export class HelloWorld {

  // Declaring the variable for binding with initial value
  yourName: string = '';
  yourPattern: string = '';
  yourNotif: string = '';

}
